import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import Layout from "./Layout/Layout";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import AccountPage from "./Pages/AccountPage/AccountPage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import BookTicketPage from "./Pages/BookTicketPage/BookTicketPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout contentPage={<HomePage />} />} />
          <Route
            path="/detail/:id/:alias"
            element={<Layout contentPage={<DetailPage />} />}
          />
          <Route
            path="/ticket-room/:showtimeID"
            element={<Layout contentPage={<BookTicketPage />} />}
          />
          <Route
            path="/login"
            element={<Layout contentPage={<LoginPage />} />}
          />
          <Route
            path="/register"
            element={<Layout contentPage={<RegisterPage />} />}
          />
          <Route
            path="/account"
            element={<Layout contentPage={<AccountPage />} />}
          />
          {/* 404 page */}
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
