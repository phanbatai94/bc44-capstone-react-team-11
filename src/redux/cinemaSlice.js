import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  cinemaList: [],
};

const cinemaSlice = createSlice({
  name: "cinemaSlice",
  initialState,
  reducers: {
    setCinemaList: (state, action) => {
      state.cinemaList = action.payload;
    },
  },
});

export const { setCinemaList } = cinemaSlice.actions;

export default cinemaSlice.reducer;
