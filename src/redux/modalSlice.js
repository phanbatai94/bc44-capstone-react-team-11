import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isOpen: false,
  playerHeight: "33.75vw",
};

const modalSlice = createSlice({
  name: "modalSlice",
  initialState,
  reducers: {
    openModal: (state) => {
      state.isOpen = true;
    },
    closeModal: (state) => {
      state.isOpen = false;
    },
    setPlayerHeight: (state, action) => {
      state.playerHeight = action.payload;
    },
  },
});

export const { openModal, closeModal, setPlayerHeight } = modalSlice.actions;

export default modalSlice.reducer;
