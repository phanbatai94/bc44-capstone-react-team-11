import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  movieList: [],
  movieInfo: "",
  movieDetail: "",
  maxPoint: 5,
};

const movieSlice = createSlice({
  name: "movieSlice",
  initialState,
  reducers: {
    setMovieList: (state, action) => {
      state.movieList = action.payload;
    },

    setMovieInfo: (state, action) => {
      state.movieInfo = action.payload;
    },

    setMovieDetail: (state, action) => {
      state.movieDetail = action.payload;
    },

    setMaxPoint: (state, action) => {
      state.maxPoint = action.payload;
    },
  },
});

export const { setMovieList, setMovieInfo, setMovieDetail, setMaxPoint } =
  movieSlice.actions;

export default movieSlice.reducer;
