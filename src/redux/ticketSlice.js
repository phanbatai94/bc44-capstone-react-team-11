import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  showtimeInfo: "",
  seatList: [],
  isChoose: false,
  chooseList: [],
};

const ticketSlice = createSlice({
  name: "ticketSlice",
  initialState,
  reducers: {
    setShowtimeInfo: (state, action) => {
      state.showtimeInfo = action.payload;
    },

    setSeatList: (state, action) => {
      state.seatList = action.payload;
    },

    setIsChoose: (state, action) => {
      state.isChoose = action.payload;
    },

    setChooseList: (state, action) => {
      let chooseList = [...state.chooseList];

      let index = chooseList.findIndex(
        (chooseSeat) => chooseSeat.maGhe === action.payload.maGhe
      );

      if (index !== -1) {
        chooseList.splice(index, 1);
      } else {
        chooseList.push(action.payload);
      }

      state.chooseList = chooseList;
    },

    clearChooseList: (state) => {
      state.chooseList = [];
    },
  },
});

export const {
  setShowtimeInfo,
  setSeatList,
  setIsChoose,
  setChooseList,
  clearChooseList,
} = ticketSlice.actions;

export default ticketSlice.reducer;
