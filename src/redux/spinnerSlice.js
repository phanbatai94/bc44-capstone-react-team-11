import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
};

const spinnerSlice = createSlice({
  name: "spinnerSlice",
  initialState,
  reducers: {
    openLoading: (state, action) => {
      state.isLoading = true;
    },
    closeLoading: (state, action) => {
      state.isLoading = false;
    },
  },
});

export const { openLoading, closeLoading } = spinnerSlice.actions;

export default spinnerSlice.reducer;
