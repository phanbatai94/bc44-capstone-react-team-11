import { createSlice } from "@reduxjs/toolkit";
import { localServ } from "../Service/localStoreService";

const initialState = {
  userLogin: localServ.getUser(),
  accessToken: localServ.getAccessToken(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserLogin: (state, action) => {
      state.userInfo = action.payload;
    },

    setAccessToken: (state, action) => {
      state.accessToken = action.payload;
    },
  },
});

export const { setUserLogin, setAccessToken } = userSlice.actions;

export default userSlice.reducer;
