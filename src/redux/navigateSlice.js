import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  navigateList: [
    { name: "Thịnh hành", href: "/#top" },
    { name: "Mới & Phổ biến", href: "/#new-movies" },
    { name: "Lịch chiếu theo rạp", href: "/#cinemas" },
  ],

  serviceList: [
    {
      name: "Chính sách bảo mật",
      href: "/",
    },
    {
      name: "Điều khoản sử dụng",
      href: "/",
    },
    {
      name: "Ngôn ngữ",
      href: "/",
    },
    {
      name: "FAQ",
      href: "/",
    },
  ],

  openStatus: false,
};

const navigateSlice = createSlice({
  name: "navigateSlice",
  initialState,
  reducers: {
    setOpenStatus: (state, action) => {
      state.openStatus = action.payload;
    },
  },
});

export const { setOpenStatus } = navigateSlice.actions;

export default navigateSlice.reducer;
