import React from "react";
import { Form, Input, message } from "antd";
import { https } from "../../Service/config";
import { useNavigate } from "react-router-dom";
import { NavLink } from "react-router-dom";

export default function RegisterPage() {
  const [form] = Form.useForm();

  const navigate = useNavigate();

  const onFinish = (values) => {
    https
      .post("/api/QuanLyNguoiDung/DangKy", values)
      .then((res) => {
        message.success("Đăng ký thành công");

        setTimeout(() => {
          navigate("/login");
        }, 1000);
      })
      .catch((err) => {
        message.error("Tài khoản đã tồn tại");
        console.log(err);
      });
  };

  return (
    <section className="movie-bg">
      <div className="container py-32 flex items-start justify-center relative z-10">
        <div className="w-full sm:w-2/3 md:w-1/2 lg:w-1/3 text-slate-100 bg-slate-900 border border-slate-700 p-6 rounded-md drop-shadow-lg">
          <h2 className="text-xl font-bold text-center mb-4">Đăng ký</h2>
          <Form
            form={form}
            name="control-hooks"
            onFinish={onFinish}
            layout="vertical"
          >
            <Form.Item
              name="taiKhoan"
              label="Tài khoản"
              rules={[
                {
                  required: true,
                  message: "Tài khoản không được để trống",
                },
              ]}
            >
              <Input id="taiKhoan" />
            </Form.Item>
            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Mật khẩu không được để trống",
                },
              ]}
            >
              <Input.Password id="matKhau" />
            </Form.Item>
            <Form.Item
              name="hoTen"
              label="Họ Tên"
              rules={[
                {
                  required: true,
                  message: "Họ Tên không được để trống",
                },
              ]}
            >
              <Input id="hoTen" />
            </Form.Item>
            <Form.Item
              name="email"
              label="Email"
              rules={[
                {
                  required: true,
                  message: "Email không được để trống",
                },
              ]}
            >
              <Input id="email" />
            </Form.Item>
            <Form.Item
              name="soDt"
              label="Số điện thoại"
              rules={[
                {
                  required: true,
                  message: "Số điện thoại không được để trống",
                },
              ]}
            >
              <Input id="soDt" />
            </Form.Item>

            <Form.Item>
              <button
                className="btn btn-primary w-full text-base"
                type="submit"
              >
                Đăng ký
              </button>
            </Form.Item>
          </Form>
          <div className="text-right">
            <p>
              Bạn đã có tài khoản? &ndash;{" "}
              <NavLink
                to={"/login"}
                className="text-blue-500 hover:text-blue-600 font-medium"
              >
                Đăng nhập
              </NavLink>
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}
