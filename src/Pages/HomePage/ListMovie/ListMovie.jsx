import React from "react";
import { Desktop, Mobile, Tablet } from "../../../responsive/responsive";
import ListMovieDesktop from "./ListMovieDesktop";
import ListMovieTablet from "./ListMovieTablet";

export default function ListMovie() {
  return (
    <section
      id="new-movies"
      className="container movie-list__content"
    >
      <h2 className="section-title">Mới & Phổ biến</h2>

      <Desktop>
        <ListMovieDesktop />
      </Desktop>

      <Tablet>
        <ListMovieTablet />
      </Tablet>

      <Mobile>
        <ListMovieTablet />
      </Mobile>
    </section>
  );
}
