import React from "react";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import moment from "moment";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function ListMovieTablet() {
  const { movieList } = useSelector((state) => state.movieSlice);

  //setting of slick slider
  const settings = {
    className: "center",
    centerMode: true,
    centerPadding: "0",
    infinite: false,
    initialSlide: 0,
    speed: 500,
    slidesPerRow: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesPerRow: 2,
          infinite: true,
          centerPadding: "30px",
          arrows: false,
        },
      },
      {
        breakpoint: 460,
        settings: {
          slidesPerRow: 1,
          infinite: true,
          centerPadding: "40px",
          arrows: false,
        },
      },
    ],
  };

  //render movie
  let renderMovieList = () => {
    //get max point
    let maxPoint = Math.max(...movieList.map((item) => item.danhGia));

    // filter 5 hot movie of hero section
    let hotMovie = movieList.filter((item) => item.hot === true);
    let movieFiltered = movieList.filter(
      (item) => !hotMovie.slice(0, 5).includes(item)
    );

    //render after filtering
    return movieFiltered.map((movie) => {
      return (
        <NavLink
          to={`/detail/${movie.maPhim}/${movie.biDanh}`}
          key={movie.maPhim}
          className="movie-container"
        >
          <div className="movie-card__tablet">
            <img src={movie.hinhAnh} alt={movie.tenPhim} />
            {/* hot badge */}
            {movie.hot ? (
              <span className="movie-badge absolute top-0 right-0">
                Thịnh hành
              </span>
            ) : null}
          </div>

          <div className="mt-2">
            {/* movie title */}
            <h2 className="movie-title text-sm mb-1">{movie.tenPhim}</h2>
            {/* movie rating release date */}
            <div className="movie-info text-xs">
              <span className="movie-rating">
                ⭐ {movie.danhGia}/{maxPoint}
              </span>
              <span className="movie-release">
                {moment(movie.ngayKhoiChieu).format("DD.MM.YYYY")}
              </span>
            </div>
          </div>
        </NavLink>
      );
    });
  };

  return <Slider {...settings}>{renderMovieList()}</Slider>;
}
