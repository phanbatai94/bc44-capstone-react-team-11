import React from "react";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setMovieInfo } from "../../../redux/movieSlice";
import { openModal, setPlayerHeight } from "../../../redux/modalSlice";
import { AiFillInfoCircle } from "react-icons/ai";
import { BsFillBookmarkPlusFill } from "react-icons/bs";
import moment from "moment";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function ListMovieDesktop() {
  const { movieList, maxPoint } = useSelector((state) => state.movieSlice);

  const dispatch = useDispatch();

  //setting of slick slider
  const settings = {
    className: "center",
    centerMode: true,
    centerPadding: "0",
    infinite: false,
    speed: 500,
    rows: 2,
    slidesPerRow: 3,
    slidesToShow: 1,
  };
  
  let handlePlayerHeight = () => {
    let screenWidth = window.innerWidth;
    if (screenWidth >= 1024) {
      dispatch(setPlayerHeight("33.75vw"));
    } else {
      dispatch(setPlayerHeight("50vw"));
    }
  };

  //render movie
  let renderMovieList = () => {
    // filter 5 hot movie of hero section
    let hotMovie = movieList.filter((item) => item.hot === true);
    let movieFiltered = movieList.filter(
      (item) => !hotMovie.slice(0, 5).includes(item)
    );

    //render after filtering
    return movieFiltered.map((movie) => {
      return (
        <div key={movie.maPhim} className="movie-container">
          <div
            className="movie-card__desktop"
            style={{ backgroundImage: `url(${movie.hinhAnh})` }}
          >
            {/* hot badge */}
            {movie.hot ? (
              <span className="movie-badge absolute top-0 right-0">
                Thịnh hành
              </span>
            ) : null}

            <div className="bg-gradient-to-t from-slate-950 drop-shadow-lg px-4 pb-4 pt-16">
              {/* movie title */}
              <h2 className="movie-title mb-1">{movie.tenPhim}</h2>
              {/* movie rating release date */}
              <div className="movie-info text-sm">
                <span className="movie-rating">
                  ⭐ {movie.danhGia}/{maxPoint}
                </span>
                <span className="movie-release">
                  {moment(movie.ngayKhoiChieu).format("DD.MM.YYYY")}
                </span>
              </div>
            </div>
            {/* buttons overlay */}
            <div className="movie-card__overlay">
              {/* book ticket */}
              <NavLink
                to={`/detail/${movie.maPhim}/${movie.biDanh}`}
                className="btn btn-primary btn-icon"
              >
                <BsFillBookmarkPlusFill />
                <span>Mua vé</span>
              </NavLink>
              {/* view detail modal */}
              <button
                onClick={() => {
                  //dispatch movie info to store
                  dispatch(setMovieInfo(movie));
                  //dispatch open modal to store
                  dispatch(openModal());
                  handlePlayerHeight();
                }}
                className="btn btn-secondary btn-icon"
              >
                <AiFillInfoCircle />
                <span>Xem chi tiết</span>
              </button>
            </div>
          </div>
        </div>
      );
    });
  };

  return <Slider {...settings}>{renderMovieList()}</Slider>;
}
