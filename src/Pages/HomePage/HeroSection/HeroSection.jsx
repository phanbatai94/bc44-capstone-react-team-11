import React, { useEffect } from "react";
import { https } from "../../../Service/config";
import { Carousel } from "antd";
import "./HeroSection.scss";
import moment from "moment";
import {
  closeModal,
  openModal,
  setPlayerHeight,
} from "../../../redux/modalSlice";
import { useDispatch, useSelector } from "react-redux";
import {
  setMaxPoint,
  setMovieInfo,
  setMovieList,
} from "../../../redux/movieSlice";
import { AiFillInfoCircle } from "react-icons/ai";
import { BsFillBookmarkPlusFill } from "react-icons/bs";
import { HashLink } from "react-router-hash-link";

export default function HeroSection() {
  //state
  const { movieList, maxPoint } = useSelector((state) => state.movieSlice);

  //dispatch
  const dispatch = useDispatch();

  //call movie api
  useEffect(() => {
    https
      .get("/api/QuanLyPhim/LayDanhSachPhim/?maNhom=GP09")
      .then((res) => {
        //dispatch movie list
        dispatch(setMovieList(res.data.content));

        let maxPoint = Math.max(...res.data.content.map((item) => item.danhGia));
        
        //dispatch movie max points
        dispatch(setMaxPoint(maxPoint));
      })
      .catch((err) => {
        console.log(err);
      });
    return () => {
      dispatch(closeModal());
    };
  }, []);

  let handlePlayerHeight = () => {
    let screenWidth = window.innerWidth;
    if (screenWidth >= 1024) {
      dispatch(setPlayerHeight("33.75vw"));
    } else {
      dispatch(setPlayerHeight("50vw"));
    }
  };

  //render movie banner
  let renderMovieList = () => {
    let hotMovie = movieList.filter((item) => item.hot == true);

    return hotMovie.slice(0, 5).map((movie) => {
      return (
        <div key={movie.maPhim} className="movie-slider">
          <div className="movie-slider__wrapper">
            <div className="container movie-slider__content">
              <div className="movie-slider__info">
                {/* hot badge */}
                {movie.hot ? (
                  <span className="movie-badge">Thịnh hành</span>
                ) : null}

                {/* movie title */}
                <h2 className="movie-title text-xl lg:text-3xl mb-1 lg:mb-2">
                  {movie.tenPhim}
                </h2>

                {/* movie rating release date */}
                <div className="movie-info text-sm lg:text-base mb-3 lg:mb-4">
                  <span className="movie-rating pb-0.5">
                    ⭐ {movie.danhGia}/{maxPoint}
                  </span>
                  <span className="movie-release pb-0.5">
                    {moment(movie.ngayKhoiChieu).format("DD.MM.YYYY")}
                  </span>
                </div>

                {/* movie description */}
                <p className="text-xs sm:text-sm font-medium text-slate-300 mb-3 lg:mb-4">
                  {movie.moTa.length > 100
                    ? movie.moTa.substring(0, 100) + "..."
                    : movie.moTa}
                </p>

                {/* buttons */}
                <div className="flex justify-center sm:justify-start space-x-2 sm:space-x-4">
                  {/* book ticket */}
                  <HashLink
                    smooth
                    to={`/detail/${movie.maPhim}/${movie.biDanh}`}
                    className="btn btn-primary btn-icon text-base flex-auto sm:flex-initial"
                  >
                    <BsFillBookmarkPlusFill />
                    <span>Mua vé</span>
                  </HashLink>

                  {/* view detail modal */}
                  <button
                    onClick={() => {
                      //dispatch movie info to store
                      dispatch(setMovieInfo(movie));
                      //dispatch open modal to store
                      dispatch(openModal());
                      handlePlayerHeight();
                    }}
                    className="btn btn-secondary btn-icon text-base flex-auto sm:flex-initial"
                  >
                    <AiFillInfoCircle />
                    <span>Xem chi tiết</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div
            className="movie-banner movie-overlay"
            style={{ backgroundImage: `url(${movie.hinhAnh})` }}
          ></div>
        </div>
      );
    });
  };

  return (
    <section id="heroSection" className="relative">
      {/* Carousel */}
      <Carousel swipeToSlide={true} autoplay autoplaySpeed={5000} effect="fade">
        {renderMovieList()}
      </Carousel>
    </section>
  );
}
