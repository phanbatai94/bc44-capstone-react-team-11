import React, { useEffect } from "react";
import { https } from "../../../Service/config";
import { Tabs } from "antd";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { setCinemaList } from "../../../redux/cinemaSlice";

export default function TabsMovie() {
  const { cinemaList } = useSelector((state) => state.cinemaSlice);

  const dispatch = useDispatch();

  // call api
  useEffect(() => {
    https
      .get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP09")
      .then((res) => {
        dispatch(setCinemaList(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  // render danh sách phim
  let renderMovieList = (movieArr) => {
    const filteredMovie = movieArr.filter(
      (movie) => movie.lstLichChieuTheoPhim.length > 2
    );

    if (filteredMovie.length === 0) {
      return (
        <div className="text-lg font-medium text-white py-4">
          <p>Không có lịch chiếu.</p>
        </div>
      );
    }

    return filteredMovie.map((movie) => {
      return (
        <div
          key={movie.maPhim}
          className="flex flex-col space-y-2 lg:space-y-0 lg:flex-row lg:items-start lg:justify-start space-x-0 lg:space-x-2 xl:space-x-3"
        >
          {/* movie banner */}
          <img
            src={movie.hinhAnh}
            alt={movie.tenPhim}
            className="w-full lg:w-1/3 rounded-md"
          />
          <div className="flex flex-col space-y-2">
            {/* movie title */}
            <h5 className="font-bold text-white uppercase leading-none">
              {movie.tenPhim}
            </h5>

            {/* movie datetime */}
            {movie.lstLichChieuTheoPhim.slice(-2).map((showtimes) => {
              return (
                <NavLink
                  to={`/ticket-room/${showtimes.maLichChieu}`}
                  key={showtimes.maLichChieu}
                  className="btn-showtime"
                >
                  {moment(showtimes.ngayChieuGioChieu).format("DD/MM/YYYY")}{" "}
                  &ndash; {moment(showtimes.ngayChieuGioChieu).format("hh:mm")}
                </NavLink>
              );
            })}
          </div>
        </div>
      );
    });
  };

  //render danh sách cụm rạp
  let renderCinemas = () => {
    return cinemaList.map((cinema) => {
      return {
        key: cinema.maHeThongRap,
        label: (
          <div className="btn-tab btn-tab__top">
            <img
              className="btn-tab__icon"
              src={cinema.logo}
              alt={cinema.tenHeThongRap}
            />
            <h3>{cinema.tenHeThongRap}</h3>
          </div>
        ),
        children: (
          <Tabs
            className="movie-tab__cards"
            tabPosition="left"
            defaultActiveKey="1"
            items={cinema.lstCumRap.map((cluster) => {
              return {
                key: cluster.maCumRap.replace(/\s+/g, ""),
                label: (
                  <div className="btn-tab btn-tab__col">
                    <h4>{cluster.tenCumRap}</h4>
                    <p className="text-gray-400 truncate hidden lg:block">
                      {cluster.diaChi}
                    </p>
                  </div>
                ),
                children: renderMovieList(cluster.danhSachPhim),
              };
            })}
          />
        ),
      };
    });
  };

  return (
    <section id="cinemas" className="container movie-tab__content">
      <h2 className="section-title">Lịch chiếu theo rạp</h2>
      <Tabs
        defaultActiveKey="1"
        className="movie-tab"
        items={renderCinemas()}
      />
    </section>
  );
}
