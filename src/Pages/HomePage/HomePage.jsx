import React from "react";
import ListMovie from "./ListMovie/ListMovie";
import TabMovie from "./TabMovie/TabMovie";
import HeroSection from "./HeroSection/HeroSection";
import DetailModal from "../../Components/Modal/DetailModal/DetailModal";

export default function HomePage() {
  return (
    <>
      <DetailModal />
      <HeroSection />
      <ListMovie />
      <TabMovie />
    </>
  );
}
