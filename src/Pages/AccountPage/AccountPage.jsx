import React from "react";
import { https } from "../../Service/config";
import { useState } from "react";
import { useEffect } from "react";
import { localServ } from "../../Service/localStoreService";
import { Tabs, message } from "antd";
import moment from "moment";

export default function AccountPage() {
  if (!localServ.getUser()) {
    window.location.href = "/login";
  }

  const [accountInfo, setAccountInfo] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDT: "",
    maNhom: "",
    maLoaiNguoiDung: "",
    hoTen: "",
  });

  const accessToken = localServ.getAccessToken();

  useEffect(() => {
    https
      .post(
        `/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
        {},
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      )
      .then((response) => {
        setAccountInfo((prevAccountInfo) =>
          Object.assign({}, prevAccountInfo, response.data.content)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    https
      .put("/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", accountInfo, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((res) => {
        message.success("Cập nhật thành công");

        localServ.setUser(res.data.content);

        window.location.reload();
      })
      .catch((err) => {
        message.error("Đã có lỗi xảy ra");
        console.log(err);
      });
  };

  let renderAccountForm = () => {
    return {
      key: "1",
      label: (
        <h2 className="btn-tab text-sm lg:text-lg lg:uppercase">
          Thông tin tài khoản
        </h2>
      ),
      children: (
        <div className="flex items-start space-x-0 lg:space-x-10">
          <div className="form-visual hidden lg:block lg:w-1/4">
            <img
              src={`https://ui-avatars.com/api/?background=random&name=${accountInfo.hoTen}`}
              alt={accountInfo.hoTen}
              className="w-1/2 rounded-full border-2 border-white mx-auto"
            />
            <h3 className="text-xl font-medium text-center text-slate-200 mt-4">
              {accountInfo.taiKhoan}
            </h3>
          </div>
          <div className="form-visual w-full lg:w-3/4">
            <form onSubmit={handleSubmit}>
              <div className="mb-6 block lg:hidden">
                <img
                  src={`https://ui-avatars.com/api/?background=random&name=${accountInfo.hoTen}`}
                  alt={accountInfo.hoTen}
                  className="w-8 md:w-12 rounded-full border-2 border-white mx-auto"
                />
                <h3 className="text-lg font-medium text-center text-slate-100 mt-2">
                  {accountInfo.taiKhoan}
                </h3>
              </div>
              <div className="mb-6 hidden">
                <label
                  htmlFor="taiKhoan"
                  className="block mb-2 text-sm font-medium text-slate-900 dark:text-slate-100"
                >
                  Tài khoản
                </label>
                <input
                  value={accountInfo.taiKhoan}
                  type="text"
                  id="taiKhoan"
                  className="movie-input cursor-not-allowed"
                  required
                  disabled
                />
              </div>
              <div className="mb-6">
                <label
                  htmlFor="matKhau"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Mật khẩu
                </label>
                <input
                  value={accountInfo.matKhau}
                  onChange={(e) =>
                    setAccountInfo((prevState) => ({
                      ...prevState,
                      matKhau: e.target.value,
                    }))
                  }
                  type="password"
                  id="matKhau"
                  className="movie-input"
                  required
                />
              </div>
              <div className="mb-6">
                <label
                  htmlFor="hoTen"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Họ tên
                </label>
                <input
                  value={accountInfo.hoTen}
                  onChange={(e) =>
                    setAccountInfo((prevState) => ({
                      ...prevState,
                      hoTen: e.target.value,
                    }))
                  }
                  type="text"
                  id="hoTen"
                  className="movie-input"
                  placeholder="Vd: PBT MOVIE"
                  required
                />
              </div>
              <div className="mb-6">
                <label
                  htmlFor="email"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Email
                </label>
                <input
                  value={accountInfo.email}
                  onChange={(e) =>
                    setAccountInfo((prevState) => ({
                      ...prevState,
                      email: e.target.value,
                    }))
                  }
                  type="email"
                  id="email"
                  className="movie-input"
                  placeholder="Vd: email@pbtmovie.com"
                  required
                />
              </div>
              <div className="mb-6">
                <label
                  htmlFor="soDT"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Số điện thoại
                </label>
                <input
                  value={accountInfo.soDT}
                  onChange={(e) =>
                    setAccountInfo((prevState) => ({
                      ...prevState,
                      soDT: e.target.value,
                    }))
                  }
                  type="text"
                  id="soDT"
                  className="movie-input"
                  placeholder="Ví dụ: 01234567"
                  required
                />
              </div>
              <div>
                <button type="submit" className="btn btn-primary w-full">
                  Cập nhật
                </button>
              </div>
            </form>
          </div>
        </div>
      ),
    };
  };

  let renderTicketList = () => {
    return {
      key: "2",
      label: (
        <h2 className="btn-tab text-sm lg:text-lg lg:uppercase">
          Lịch sử đặt vé
        </h2>
      ),
      children: (
        <div className="bg-slate-900 bg-opacity-50 border border-slate-700 p-6 rounded-md drop-shadow-lg">
          <div className="relative max-h-96 overflow-y-auto rounded-sm">
            <table className="w-full text-xs md:text-sm text-left text-gray-500 dark:text-gray-400">
              <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Tên phim
                  </th>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Ngày đặt
                  </th>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Giá vé
                  </th>
                </tr>
              </thead>
              <tbody>
                {accountInfo.thongTinDatVe?.length ? (
                  accountInfo.thongTinDatVe?.map((ticket) => {
                    return (
                      <tr
                        key={ticket.maVe}
                        className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                      >
                        <th
                          scope="row"
                          className="px-3 py-2 lg:px-6 lg:-py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap"
                        >
                          {ticket.tenPhim}
                        </th>

                        <td className="px-3 py-2 lg:px-6 lg:-py-4">
                          {moment(ticket.ngayDat).format("hh:mm - DD.MM.YYYY")}
                        </td>

                        <td className="px-3 py-2 lg:px-6 lg:-py-4">
                          {ticket.giaVe.toLocaleString("vi-VN", {
                            style: "currency",
                            currency: "VND",
                          })}
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <td colSpan={3} className="px-3 py-2 lg:px-6 lg:-py-4">
                      <p>Chưa có lịch sử đặt vé, vui lòng đặt vé để xem.</p>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      ),
    };
  };

  const items = [renderAccountForm(), renderTicketList()];

  return (
    <section className="movie-bg">
      <div className="container py-24 relative z-10">
        <Tabs defaultActiveKey="1" className="movie-tab" items={items} />
      </div>

      {/* <div className="container py-24 flex flex-col items-center space-y-8 xl:space-y-0 xl:flex-row xl:items-start xl:space-x-4 relative z-10">
        <div className="w-full md:w-2/3 xl:w-1/3 bg-slate-900 border border-slate-700 p-6 rounded-md drop-shadow-lg">
          <h2 className="text-xl text-slate-100 font-bold text-center pb-4">
            Thông tin tài khoản
          </h2>
          <form onSubmit={handleSubmit}>
            <div className="mb-6">
              <label
                htmlFor="taiKhoan"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
              >
                Tài khoản
              </label>
              <input
                value={accountInfo.taiKhoan}
                type="text"
                id="taiKhoan"
                className="movie-input cursor-not-allowed"
                required
                disabled
              />
            </div>
            <div className="mb-6">
              <label
                htmlFor="matKhau"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
              >
                Mật khẩu
              </label>
              <input
                value={accountInfo.matKhau}
                onChange={(e) =>
                  setAccountInfo((prevState) => ({
                    ...prevState,
                    matKhau: e.target.value,
                  }))
                }
                type="password"
                id="matKhau"
                className="movie-input"
                required
              />
            </div>
            <div className="mb-6">
              <label
                htmlFor="hoTen"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
              >
                Họ tên
              </label>
              <input
                value={accountInfo.hoTen}
                onChange={(e) =>
                  setAccountInfo((prevState) => ({
                    ...prevState,
                    hoTen: e.target.value,
                  }))
                }
                type="text"
                id="hoTen"
                className="movie-input"
                placeholder="Vd: PBT MOVIE"
                required
              />
            </div>
            <div className="mb-6">
              <label
                htmlFor="email"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
              >
                Email
              </label>
              <input
                value={accountInfo.email}
                onChange={(e) =>
                  setAccountInfo((prevState) => ({
                    ...prevState,
                    email: e.target.value,
                  }))
                }
                type="email"
                id="email"
                className="movie-input"
                placeholder="Vd: email@pbtmovie.com"
                required
              />
            </div>
            <div className="mb-6">
              <label
                htmlFor="soDT"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
              >
                Số điện thoại
              </label>
              <input
                value={accountInfo.soDT}
                onChange={(e) =>
                  setAccountInfo((prevState) => ({
                    ...prevState,
                    soDT: e.target.value,
                  }))
                }
                type="text"
                id="soDT"
                className="movie-input"
                placeholder="Ví dụ: 01234567"
                required
              />
            </div>
            <div>
              <button type="submit" className="btn btn-primary w-full">
                Cập nhật
              </button>
            </div>
          </form>
        </div>
        <div className="w-full xl:w-2/3 bg-slate-900 border border-slate-700 p-6 rounded-md drop-shadow-lg">
          <h2 className="text-xl text-slate-100 font-bold text-center pb-4">
            Lịch sử đặt vé
          </h2>
          <div className="relative max-h-96 overflow-y-auto rounded-sm">
            <table className="w-full text-xs md:text-sm text-left text-gray-500 dark:text-gray-400">
              <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Tên phim
                  </th>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Ngày đặt
                  </th>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Giá vé
                  </th>
                </tr>
              </thead>
              <tbody>{renderTicketList()}</tbody>
            </table>
          </div>
        </div>
      </div> */}
    </section>
  );
}
