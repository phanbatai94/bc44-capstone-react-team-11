import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { https } from "../../Service/config";
import { MdChair } from "react-icons/md";
import { localServ } from "../../Service/localStoreService";
import { useDispatch, useSelector } from "react-redux";
import {
  clearChooseList,
  setChooseList,
  setIsChoose,
  setSeatList,
  setShowtimeInfo,
} from "../../redux/ticketSlice";
import ConfirmModal from "../../Components/Modal/ConfirmModal/ConfirmModal";
import { openModal } from "../../redux/modalSlice";
import _ from "lodash";
import "./BookTicketPage.scss";

export default function BookTicketPage() {
  if (!localServ.getUser()) {
    window.location.href = "/login";
  }

  const { showtimeInfo, seatList, isChoose, chooseList } = useSelector(
    (state) => state.ticketSlice
  );

  const accessToken = "Bearer " + localServ.getUser().accessToken;

  const dispatch = useDispatch();

  const { showtimeID } = useParams();

  useEffect(() => {
    https
      .get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${showtimeID}`)
      .then((res) => {
        dispatch(setShowtimeInfo(res.data.content.thongTinPhim));
        dispatch(setSeatList(res.data.content.danhSachGhe));
      })
      .catch((err) => console.log(err));

    window.scrollTo(0, 0);

    return () => {
      dispatch(setShowtimeInfo(""));
      dispatch(setSeatList([]));
      dispatch(setIsChoose(false));
      dispatch(clearChooseList());
    };
  }, []);

  let renderSeat = () => {
    return seatList.map((seat) => {
      let seatColor =
        "ticket-seat__hover " +
        (chooseList.findIndex(
          (shooseSeat) => shooseSeat.maGhe === seat.maGhe
        ) != -1
          ? "ticket-seat__choosin"
          : seat.loaiGhe === "Thuong"
          ? "ticket-seat__regular"
          : "ticket-seat__vip");

      let seatSold =
        seat.taiKhoanNguoiDat === localServ.getUser().taiKhoan
          ? "ticket-seat__user"
          : "ticket-seat__sold";

      return (
        <button
          onClick={() => {
            dispatch(setIsChoose(true));
            dispatch(setChooseList(seat));
          }}
          key={seat.maGhe}
          title={seat.tenGhe}
          className={"ticket-seat " + (seat.daDat ? seatSold : seatColor)}
          disabled={seat.daDat}
        >
          <MdChair className="ticket-seat__icon" />
        </button>
      );
    });
  };

  let handleBookTicket = (value) => {
    https
      .post("/api/QuanLyDatVe/DatVe", value, {
        headers: { Authorization: accessToken },
      })
      .then((res) => {
        //open modal
        dispatch(openModal());
        //hide toal div
        dispatch(setIsChoose(false));
        //clear choosing seat list
        dispatch(clearChooseList());
        //call api reload seat list
        https
          .get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${showtimeID}`)
          .then((res) => {
            dispatch(setSeatList(res.data.content.danhSachGhe));
          })
          .catch((err) => console.log(err));
        //scroll to top
        window.scrollTo(0, 0);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <section id="ticket-room">
      <div className="container movie-tab__content">
        <h2 className="section-title">Chọn ghế &mdash; Đặt vé</h2>

        <div className="ticket-wrapper">
          {/* seat info */}
          <div className="ticket-room">
            {/* screen */}
            <div className="ticket-screen">
              <p className="ticket-screen__text">Màn hình</p>
            </div>

            {/* seats */}
            <div className="ticket-seatlist">{renderSeat()}</div>

            {/* seat description */}
            <table className="ticket-seatdes">
              <thead>
                <tr>
                  <td>
                    <div className="ticket-seatdes__wrapper">
                      <MdChair className="ticket-seatdes__icon text-slate-400" />
                      <span className="ticket-seatdes__text">Ghế thường</span>
                    </div>
                  </td>
                  <td>
                    <div className="ticket-seatdes__wrapper">
                      <MdChair className="ticket-seatdes__icon text-yellow-400" />
                      <span className="ticket-seatdes__text">Ghế VIP</span>
                    </div>
                  </td>
                  <td>
                    <div className="ticket-seatdes__wrapper">
                      <MdChair className="ticket-seatdes__icon text-emerald-400" />
                      <span className="ticket-seatdes__text">Ghế đang chọn</span>
                    </div>
                  </td>
                  <td>
                    <div className="ticket-seatdes__wrapper">
                      <MdChair className="ticket-seatdes__icon text-red-800" />
                      <span className="ticket-seatdes__text">Ghế đã bán</span>
                    </div>
                  </td>
                  <td>
                    <div className="ticket-seatdes__wrapper">
                      <MdChair className="ticket-seatdes__icon text-blue-800" />
                      <span className="ticket-seatdes__text">Ghế của bạn</span>
                    </div>
                  </td>
                </tr>
              </thead>
            </table>
          </div>

          {/* ticket info */}
          <div className="ticket-ticket">
            <div className="ticket-content">
              <div className="ticket-poster">
                <img
                  src={showtimeInfo.hinhAnh}
                  alt={showtimeInfo.tenPhim}
                  className="w-full"
                />
              </div>
              <div className="ticket-checkout">
                {/* showtime info */}
                <h3 className="ticket-checkout__title">
                  {showtimeInfo.tenPhim}
                </h3>
                <p className="ticket-checkout__datetime">
                  {showtimeInfo.ngayChieu} &ndash; {showtimeInfo.gioChieu}
                </p>
                <p className="ticket-checkout__place">
                  {showtimeInfo.tenCumRap} &ndash; {showtimeInfo.tenRap}
                </p>

                {/* ticket total */}
                <div className={isChoose ? "block" : "hidden"}>
                  <hr className="border-slate-700 my-2" />

                  <div className="ticket-checkout__listed">
                    <div className="ticket-checkout__choosen">
                      <p>Ghế đã chọn:</p>
                      <p className="ticket-checkout__seatlist">
                        {chooseList
                          ? _.sortBy(chooseList, ["stt"]).map((seat, index) => {
                              return index !== chooseList.length - 1 ? (
                                <span key={index}>{seat.tenGhe}, </span>
                              ) : (
                                <span key={index}>{seat.tenGhe}</span>
                              );
                            })
                          : null}
                      </p>
                    </div>

                    <div className="ticket-checkout__total">
                      <p>Tổng tiền:</p>
                      <p className="ticket-checkout__price">
                        {chooseList
                          .reduce((total, seat) => {
                            return (total += parseInt(seat.giaVe));
                          }, 0)
                          .toLocaleString("vi-VN", {
                            style: "currency",
                            currency: "VND",
                          })}
                      </p>
                    </div>
                  </div>

                  <div className="ticket-action">
                    <button
                      onClick={() => {
                        handleBookTicket({
                          maLichChieu: showtimeID,
                          danhSachVe: chooseList,
                        });
                      }}
                      className="btn btn-primary w-full"
                    >
                      Đặt vé
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <ConfirmModal />
    </section>
  );
}
