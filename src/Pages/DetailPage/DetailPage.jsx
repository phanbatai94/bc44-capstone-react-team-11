import "./DetailPage.scss";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { https } from "../../Service/config";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { AiOutlineArrowDown, AiFillPlayCircle } from "react-icons/ai";
import { setMovieDetail, setMovieInfo } from "../../redux/movieSlice";
import { openModal, setPlayerHeight } from "../../redux/modalSlice";
import { HashLink } from "react-router-hash-link";
import TrailerModal from "../../Components/Modal/TrailerModal/TrailerModal";
import { Progress, Tabs } from "antd";
import { NavLink } from "react-router-dom";

export default function DetailPage() {
  // get movie data from redux
  const { movieDetail, maxPoint } = useSelector((state) => state.movieSlice);

  const dispatch = useDispatch();

  // get movie id from browser
  let { id } = useParams();

  //call api with movie id
  useEffect(() => {
    https
      .get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`)
      .then((res) => {
        //set movie data to redux
        dispatch(setMovieDetail(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });

    window.scrollTo(0, 0);
  }, []);

  let handlePlayerHeight = () => {
    let screenWidth = window.innerWidth;
    if (screenWidth >= 1024) {
      dispatch(setPlayerHeight("33.75vw"));
    } else {
      dispatch(setPlayerHeight("50vw"));
    }
  };

  //render cinemas list
  let renderCinemas = () => {
    return movieDetail.heThongRapChieu?.length !== 0
      ? movieDetail.heThongRapChieu?.map((cinemas) => {
          return {
            key: cinemas.maHeThongRap,
            label: (
              <div className="btn-tab btn-tab__top">
                <img
                  className="btn-tab__icon"
                  src={cinemas.logo}
                  alt={cinemas.tenHeThongRap}
                />
                <h3>{cinemas.tenHeThongRap}</h3>
              </div>
            ),
            children: cinemas.cumRapChieu.map((cinema) => {
              return (
                <div key={cinema.maCumRap.replace(/\s+/g, "")} className="mb-6">
                  <div className="mb-3">
                    <h4 className="text-sm sm:text-base text-slate-200 font-medium">
                      {cinema.tenCumRap}
                    </h4>
                    <p className="text-xs sm:text-sm text-slate-400">{cinema.diaChi}</p>
                  </div>
                  <div className="flex items-center flex-wrap">
                    {renderShowTime(cinema.lichChieuPhim)}
                  </div>
                </div>
              );
            }),
          };
        })
      : [
          {
            key: "showtime-null",
            label: (
              <div className="bg-slate-900 hover:bg-slate-800 transition-all py-3 px-4 rounded-md">
                <h3 className="text-lg font-medium text-slate-100 uppercase">
                  Rạp chiếu nghỉ
                </h3>
              </div>
            ),
            children: (
              <div className="text-lg font-medium text-slate-100 py-4">
                <p>Không có lịch chiếu.</p>
              </div>
            ),
          },
        ];
  };

  //render showtimes list
  let renderShowTime = (showtimeArr) => {
    return showtimeArr.map((showtime) => {
      return (
        <NavLink
          to={`/ticket-room/${showtime.maLichChieu}`}
          key={showtime.maLichChieu}
          className="btn-showtime"
        >
          {showtime.tenRap} &ndash;{" "}
          {moment(showtime.ngayChieuGioChieu).format("DD/MM/YYYY")} &ndash;{" "}
          {moment(showtime.ngayChieuGioChieu).format("hh:mm")}
        </NavLink>
      );
    });
  };

  return (
    <section id="detail-movie">
      {/* movie cover */}
      <div
        className="movie-banner movie-overlay"
        style={{ backgroundImage: `url(${movieDetail.hinhAnh})` }}
      />

      {/* movie details */}
      <div className="movie-content">
        <div className="container">
          {/* hot badge */}
          {movieDetail.hot ? (
            <span className="movie-badge">Thịnh hành</span>
          ) : null}

          {/* movie title */}
          <h2 className="movie-title text-3xl mb-2 md:mb-4">{movieDetail.tenPhim}</h2>

          {/* movie rating release date */}
          <div className="block md:hidden">
            <div className="movie-info text-sm lg:text-base mb-3 lg:mb-4">
              <span className="movie-rating pb-0.5">
                ⭐ {movieDetail.danhGia}/{maxPoint}
              </span>
              <span className="movie-release pb-0.5">
                {moment(movieDetail.ngayKhoiChieu).format("DD.MM.YYYY")}
              </span>
            </div>
          </div>

          {/* buttons */}
          <div className="flex justify-center sm:justify-start space-x-2 sm:space-x-4 mb-6 md:mb-8">
            {/* choose cinema */}
            <HashLink
              smooth
              to={`/detail/${movieDetail.maPhim}/${movieDetail.biDanh}/#showtimes`}
              className="btn btn-primary btn-icon text-base flex-auto sm:flex-initial"
            >
              <AiOutlineArrowDown className="w-5 h-5" />
              <span>Chọn rạp</span>
            </HashLink>
            {/* watch trailer */}
            <button
              onClick={() => {
                //dispatch movie info to store
                dispatch(setMovieInfo(movieDetail));
                //dispatch open modal to store
                dispatch(openModal());
                handlePlayerHeight();
              }}
              className="btn btn-secondary btn-icon text-base flex-auto sm:flex-initial"
            >
              <AiFillPlayCircle className="w-5 h-5" />
              <span>Xem Trailer</span>
            </button>
          </div>

          <div className="flex justify-start space-x-0 md:space-x-6">
            {/* movie rating */}
            <Progress
            className="hidden md:block"
              type="circle"
              strokeColor={{ "0%": "#5EA5F9", "100%": "#11B886" }}
              percent={(movieDetail.danhGia / maxPoint) * 100}
              format={() => (
                <div>
                  <h3 className="text-sm font-medium text-slate-300 mb-1">
                    Đánh giá
                  </h3>
                  <p className="font-medium text-slate-100">
                    ⭐{movieDetail.danhGia}/{maxPoint}
                  </p>
                </div>
              )}
            />
            <div className="pl-4 border-l-2 border-l-slate-800">
              {/* movie release date */}
              <div className="hidden md:block">
                <h3 className="text-sm sm:text-base font-medium text-slate-100">Ngày khởi chiếu</h3>
                <p className="text-xs sm:text-sm text-slate-400 mb-3">
                  {moment(movieDetail.ngayKhoiChieu).format("DD.MM.YYYY")}
                </p>
              </div>
              {/* movie story line */}
              <h3 className="font-medium text-slate-100">Cốt truyện</h3>
              <p className="text-slate-400">{movieDetail.moTa}</p>
            </div>
          </div>
        </div>
      </div>

      {/* showtimes */}
      <div id="showtimes">
        <div className="container movie-tab__content">
          <h2 className="section-title">Lịch chiếu</h2>
          <Tabs
            className="movie-tab"
            defaultActiveKey="1"
            items={renderCinemas()}
          />
        </div>
      </div>

      {/* trailer modal */}
      <TrailerModal />
    </section>
  );
}
