import React from "react";
import { Form, Input, message } from "antd";
import { https } from "../../Service/config";
import { useDispatch } from "react-redux";
import { setUserLogin } from "../../redux/userSlice";
import { useNavigate } from "react-router-dom";
import { localServ } from "../../Service/localStoreService";
import { NavLink } from "react-router-dom";

export default function LoginPage() {  
  if (localServ.getUser()) {
    window.location.href = "/";
  }

  let dispatch = useDispatch();

  let navigate = useNavigate();

  const onFinish = (values) => {
    https
      .post("/api/QuanLyNguoiDung/DangNhap", values)
      .then((res) => {
        message.success("Đăng nhập thành công");

        dispatch(setUserLogin(res.data.content));

        localServ.setUser(res.data.content);
        
        localServ.setAccessToken(res.data.content.accessToken);

        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  return (
    <section className="movie-bg">
      <div className="container py-32 flex items-start justify-center relative z-10">
        <div className="w-full sm:w-2/3 md:w-1/2 lg:w-1/3 text-slate-100 bg-slate-900 border border-slate-700 p-6 rounded-md drop-shadow-lg">
          <h2 className="text-xl font-bold text-center mb-4">
            Đăng nhập
          </h2>

          <Form onFinish={onFinish} layout="vertical">
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Tài khoản",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Mật khẩu",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item>
              <button className="btn btn-primary w-full text-base" type="submit">
                Đăng nhập
              </button>
            </Form.Item>
          </Form>
          
          <div className="text-right">
            <p>
              Bạn chưa có tài khoản? &ndash;{" "}
              <NavLink
                to={"/register"}
                className="text-blue-500 hover:text-blue-600 font-medium"
              >
                Đăng ký
              </NavLink>
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}
