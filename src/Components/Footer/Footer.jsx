import React from "react";
import "./Footer.scss";
import { HashLink } from "react-router-hash-link";
import { NavLink } from "react-router-dom";
import moment from "moment";
import { useSelector } from "react-redux";
import {
  AiFillFacebook,
  AiOutlineInstagram,
  AiOutlineTwitter,
  AiFillYoutube,
} from "react-icons/ai";

let socialList = [
  {
    name: "Facebook",
    href: "/",
    icon: <AiFillFacebook />,
  },
  {
    name: "Instagram",
    href: "/",
    icon: <AiOutlineInstagram />,
  },
  {
    name: "Twitter",
    href: "/",
    icon: <AiOutlineTwitter />,
  },
  {
    name: "Youtube",
    href: "/",
    icon: <AiFillYoutube />,
  },
];

export default function Footer() {
  // get navigation list from redux
  const { navigateList, serviceList } = useSelector(
    (state) => state.navigateSlice
  );

  //render navigation
  let renderNavigation = () => {
    return navigateList.map((item, key) => {
      return (
        <li key={`nav-item-${key}`}>
          {key > 0 ? <span className="hidden sm:inline">/</span> : null}

          <HashLink smooth to={item.href}>
            <span>{item.name}</span>
          </HashLink>
        </li>
      );
    });
  };

  let renderSocial = () => {
    return socialList.map((item, key) => {
      return (
        <li key={`cocial-item-${key}`}>
          <NavLink to={item.href} title={item.name}>
            {item.icon}
          </NavLink>
        </li>
      );
    });
  };

  let renderService = () => {
    return serviceList.map((item, key) => {
      return (
        <li key={`service-item-${key}`}>
          <NavLink to={item.href}>{item.name}</NavLink>
        </li>
      );
    });
  };

  return (
    <footer className="footer">
      <div className="container">
        <div className="footer-content">
          <div className="footer-links">
            <ul className="footer-pagenav">{renderNavigation()}</ul>
            <ul className="footer-social">{renderSocial()}</ul>
          </div>
          <div className="footer-slogan">
            <h2>
              Nền tảng đặt vé của chúng tôi <br className="hidden lg:block" />
              được hàng triệu người tin dùng <br className="hidden lg:block" />
              và mang đến những bộ phim <br className="hidden lg:block" />
              mới được cập nhật hay nhất <br className="hidden lg:block" />
              trên toàn thế giới.
            </h2>
          </div>
        </div>
        <div className="footer-bottom">
          <ul className="footer-service">{renderService()}</ul>
          <p className="footer-copyright">
            &copy; {moment().format("y")} &mdash; PBT, Inc.
          </p>
        </div>
      </div>
    </footer>
  );
}
