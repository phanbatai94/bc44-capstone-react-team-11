import React from 'react'
import { Desktop, Tablet } from '../../../responsive/responsive'
import PageNavDesktop from './PageNavDesktop'
import PageNavTablet from './PageNavTablet'

export default function PageNav() {
  return (
    <nav className='page-nav'>
      <Desktop>
        <PageNavDesktop />
      </Desktop>

      <Tablet>
        <PageNavTablet />
      </Tablet>
    </nav>
  )
}
