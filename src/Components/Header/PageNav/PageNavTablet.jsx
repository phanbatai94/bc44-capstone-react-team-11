import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setOpenStatus } from "../../../redux/navigateSlice";
import { HashLink } from "react-router-hash-link";
import { NavLink } from "react-router-dom";
import { localServ } from "../../../Service/localStoreService";

export default function PageNavTablet() {
  const { openStatus, navigateList } = useSelector(
    (state) => state.navigateSlice
  );

  const userLogin = localServ.getUser();

  let handleLogout = () => {
    localServ.removeUser();
    localServ.removeAccessToken();
    window.location.href = "/";
  };

  const dispatch = useDispatch();

  let renderNavigate = () => {
    return navigateList.map((item, key) => {
      return (
        <li key={`item-${key}`}>
          <HashLink
            onClick={() => {
              dispatch(setOpenStatus(!openStatus));
            }}
            smooth
            to={item.href}
          >
            {item.name}
          </HashLink>
        </li>
      );
    });
  };

  return (
    <div className={`page-nav__tablet ${openStatus ? "open" : ""}`}>
      <div className="container">
        {/* navigate */}
        <ul className="page-nav__menu">{renderNavigate()}</ul>

        {/* user */}
        {userLogin ? (
          <ul className="page-nav__menu border-t border-t-slate-800">
            <li>
              <NavLink
                onClick={() => {
                  dispatch(setOpenStatus(!openStatus));
                }}
                to={"/account"}
              >
                Tài khoản
              </NavLink>
            </li>
            <li>
              <NavLink
                onClick={() => {
                  handleLogout();
                }}
                to={"/"}
              >
                Đăng xuất
              </NavLink>
            </li>
          </ul>
        ) : (
          <ul className="page-nav__menu border-t border-t-slate-800">
            <li>
              <NavLink
                onClick={() => {
                  dispatch(setOpenStatus(!openStatus));
                }}
                to={"/login"}
              >
                Đăng nhập
              </NavLink>
            </li>
            <li>
              <NavLink
                onClick={() => {
                  dispatch(setOpenStatus(!openStatus));
                }}
                to={"/register"}
              >
                Đăng ký
              </NavLink>
            </li>
          </ul>
        )}
      </div>
    </div>
  );
}
