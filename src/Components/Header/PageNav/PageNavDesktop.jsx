import React from "react";
import { useSelector } from "react-redux";
import { HashLink } from "react-router-hash-link";

export default function PageNavDesktop() {
  // create navigation list
  const { navigateList } = useSelector((state) => state.navigateSlice);
  
  //render navigation
  let renderNavigation = () => {
    return navigateList.map((item, key) => {
      return (
        <li key={`item-${key}`}>
          <HashLink
            className="hover:text-emerald-400 transition-all"
            smooth
            to={item.href}
          >
            {item.name}
          </HashLink>
        </li>
      );
    });
  };

  return (
    <ul className="page-nav__desktop">
      {renderNavigation()}
    </ul>
  );
}
