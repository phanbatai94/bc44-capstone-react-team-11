import React from "react";
import { localServ } from "../../../Service/localStoreService";
import { NavLink } from "react-router-dom";
import { BsChevronDown } from "react-icons/bs";

export default function UserNav() {
  const userLogin = localServ.getUser();

  let handleLogout = () => {
    localServ.removeUser();

    localServ.removeAccessToken();
    
    window.location.href = "/";
  };

  return userLogin ? (
    <div className="user-dropdown relative">
      {/* dropdown button */}
      <button className="inline-flex items-center space-x-2">
        {/* avatar */}
        <div className="w-8 h-8 bg-slate-500 border-2 border-white rounded-full overflow-hidden outline-none">
          <img
            src={`https://ui-avatars.com/api/?background=random&name=${userLogin.hoTen}`}
            alt={userLogin.hoTen}
            className="w-full h-full object-cover"
          />
        </div>
        <BsChevronDown />
      </button>

      {/* dropdown menu */}
      <ul className="user-menu">
        <li>
          <span>{userLogin.hoTen}</span>
        </li>
        <li>
          <NavLink to="/account">Tài khoản</NavLink>
        </li>
        <li>
          <button onClick={handleLogout}>Đăng xuất</button>
        </li>
      </ul>
    </div>
  ) : (
    <ul className="flex items-center justify-end space-x-3 font-medium">
      <li>
        <NavLink to={"/register"} className="btn btn-secondary">
          Đăng ký
        </NavLink>
      </li>
      <li>
        <NavLink to={"/login"} className="btn btn-primary">
          Đăng nhập
        </NavLink>
      </li>
    </ul>
  );
}
