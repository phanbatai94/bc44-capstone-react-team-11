import React, { useEffect, useState } from "react";
import { Desktop, Mobile, Tablet } from "../../responsive/responsive";
import HeaderDesktop from "./HeaderDesktop";
import HeaderTablet from "./HeaderTablet";

export default function Header() {
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    const onScroll = () => setOffset(window.pageYOffset);

    window.removeEventListener("scroll", onScroll);

    window.addEventListener("scroll", onScroll, { passive: true });

    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  return (
    <header
      id="mainHeader"
      className={`main-header ${offset > 0 ? "header-onscroll" : ""}`}
    >
      <Desktop>
        <HeaderDesktop />
      </Desktop>

      <Tablet>
        <HeaderTablet />
      </Tablet>

      <Mobile>
        <HeaderTablet />
      </Mobile>
    </header>
  );
}
