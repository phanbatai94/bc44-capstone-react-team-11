import React from "react";
import { NavLink } from "react-router-dom";
import "./Header.scss";
import PageNav from "./PageNav/PageNav";
import UserNav from "./UserNav/UserNav";

export default function HeaderDesktop() {

  return (
      <div className="container flex items-center justify-between drop-shadow">
        {/* logo */}
        <NavLink to={"/"}>
          <h1 className="brand">
            <span className="brand-corp">PBT</span>
            <span className="brand-sub">
              MOVIE
            </span>
          </h1>
        </NavLink>

        {/* page navigation */}
        <PageNav />

        {/* user navigation  */}
        <UserNav />
      </div>
  );
}
