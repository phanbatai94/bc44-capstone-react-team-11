import React from "react";
import { NavLink } from "react-router-dom";
import { HiBars3 } from "react-icons/hi2";
import PageNavTablet from "./PageNav/PageNavTablet";
import { useDispatch, useSelector } from "react-redux";
import { setOpenStatus } from "../../redux/navigateSlice";
import { localServ } from "../../Service/localStoreService";

export default function HeaderTablet() {
  const dispatch = useDispatch();

  const userLogin = localServ.getUser();

  const { openStatus } = useSelector((state) => state.navigateSlice);

  openStatus
    ? document.body.classList.add("overflow-y-hidden")
    : document.body.classList.remove("overflow-y-hidden");

  return (
    <div className="container">
      <div className="flex items-center justify-between drop-shadow">
        {/* logo */}
        <NavLink
          onClick={() => {
            dispatch(setOpenStatus(false));
          }}
          to={"/"}
        >
          <h1 className="brand">
            <span className="brand-corp">PBT</span>
            <span className="brand-sub">MOVIE</span>
          </h1>
        </NavLink>

        {/* burger button */}
        <button
          onClick={() => {
            dispatch(setOpenStatus(!openStatus));
          }}
          className="btn-nav"
        >
          {/* avatar */}
          {userLogin ? (
            <div className="btn-nav__avatar">
              <img
                src={`https://ui-avatars.com/api/?background=random&name=${userLogin.hoTen}`}
                alt={userLogin.hoTen}
                className="w-full h-full object-cover"
              />
            </div>
          ) : null}
          <HiBars3 className="btn-nav__icon" />
        </button>
      </div>

      {/* hidden nav */}
      <PageNavTablet />
    </div>
  );
}
