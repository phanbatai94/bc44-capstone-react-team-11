import React from "react";
import { useSelector } from "react-redux";
import { GridLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);

  return isLoading ? (
    <div className="fixed w-screen h-screen z-50 grid place-content-center bg-gray-950">
      <GridLoader color="#10B981" />
    </div>
  ) : null;
}
