import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../../redux/modalSlice";
import ReactPlayer from "react-player";
import { AiOutlineClose } from "react-icons/ai";

export default function TrailerModal() {
  const dispatch = useDispatch();

  //get redux
  const { movieInfo } = useSelector((state) => state.movieSlice);

  const { isOpen, playerHeight } = useSelector((state) => state.modalSlice);

  isOpen
    ? document.body.classList.add("overflow-y-hidden")
    : document.body.classList.remove("overflow-y-hidden");

  return isOpen ? (
    <div
      onClick={() => dispatch(closeModal())}
      className="modal-overlay"
    >
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className="modal-wrapper"
      >
        {/*content*/}
        <div className="modal-content">
          {/*header*/}
          <div className="modal-player">
            {/* trailer frame */}
            <ReactPlayer
              url={movieInfo.trailer}
              width="100%"
              height={playerHeight}
              controls
              playing
              loop
            />

            {/* close button */}
            <button
              title="Đóng"
              className="btn-close"
              onClick={() => dispatch(closeModal())}
            >
              <AiOutlineClose className="h-5 w-5" />
            </button>
          </div>
        </div>
      </div>
    </div>
  ) : null;
}
