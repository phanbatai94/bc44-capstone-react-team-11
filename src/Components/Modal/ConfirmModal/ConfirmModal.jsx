import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../../redux/modalSlice";
import Lottie from "lottie-react";
import animation_check from "./animation_check.json";

export default function ConfirmModal() {
  const dispatch = useDispatch();

  const { isOpen } = useSelector((state) => state.modalSlice);

  let handleOnConfirm = () => {
    dispatch(closeModal());
  };

  return isOpen ? (
    <div className="modal-overlay">
      <div className="modal-wrapper">
        {/*content*/}
        <div className="modal-content p-6">
          {/*header*/}
          <Lottie animationData={animation_check} loop={false} className="w-1/3 lg:w-1/4 mx-auto" />

          {/*body*/}
          <p className="text-xl text-center font-medium text-slate-100 mt-1 mb-10">
            Đặt vé thành công
          </p>

          {/*footer*/}
          <button
            onClick={() => {
              handleOnConfirm();
            }}
            className="btn btn-primary"
          >
            Xác nhận
          </button>
        </div>
      </div>
    </div>
  ) : null;
}
