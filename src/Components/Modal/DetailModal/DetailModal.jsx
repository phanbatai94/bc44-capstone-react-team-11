import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../../redux/modalSlice";
import ReactPlayer from "react-player";
import { NavLink } from "react-router-dom";
import { AiOutlineClose } from "react-icons/ai";
import { BsFillBookmarkPlusFill } from "react-icons/bs";
import moment from "moment";

export default function DetailModal() {
  const dispatch = useDispatch();

  //get redux
  const { movieInfo, maxPoint } = useSelector((state) => state.movieSlice);

  const { isOpen, playerHeight } = useSelector((state) => state.modalSlice);

  isOpen
    ? document.body.classList.add("overflow-y-hidden")
    : document.body.classList.remove("overflow-y-hidden");

  return isOpen ? (
    <>
      <div onClick={() => dispatch(closeModal())} className="modal-overlay">
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
          className="modal-wrapper"
        >
          {/*content*/}
          <div className="modal-content">
            {/*header*/}
            <div className="modal-player modal-player__ombre">
              {/* trailer frame */}
              <ReactPlayer
                url={movieInfo.trailer}
                width="100%"
                height={playerHeight}
                playing
                loop
              />

              {/* close button */}
              <button
                title="Đóng"
                className="btn-close"
                onClick={() => dispatch(closeModal())}
              >
                <AiOutlineClose className="h-5 w-5" />
              </button>
            </div>

            {/*body*/}
            <div className="modal-info">
              {/* hot badge */}
              {movieInfo.hot ? (
                <span className="movie-badge">Thịnh hành</span>
              ) : null}

              {/* movie title */}
              <h2 className="movie-title text-xl lg:text-3xl mb-1 lg:mb-2">
                {movieInfo.tenPhim}
              </h2>

              <div className="movie-info text-sm lg:text-base mb-3 lg:mb-4">
                {/* movie rating */}
                <span className="movie-rating">
                  ⭐ {movieInfo.danhGia}/{maxPoint}
                </span>

                {/* movie relese day */}
                <span className="movie-release">
                  {moment(movieInfo.ngayKhoiChieu).format("DD.MM.YYYY")}
                </span>
              </div>

              <div className="flex mb-3 lg:mb-6">
                {/* book ticket button */}
                <NavLink
                  to={`/detail/${movieInfo.maPhim}/${movieInfo.biDanh}/#showtimes`}
                  className="btn btn-primary btn-icon flex-auto md:flex-initial"
                >
                  <BsFillBookmarkPlusFill />
                  <span>Mua vé</span>
                </NavLink>
              </div>

              {/* movie description */}
              <p className="text-sm lg:text-base font-medium text-gray-300">
                {movieInfo.moTa}
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  ) : null;
}
