import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";
import userSlice from "./redux/userSlice";
import movieSlice from "./redux/movieSlice";
import spinnerSlice from "./redux/spinnerSlice";
import modalSlice from "./redux/modalSlice";
import ticketSlice from "./redux/ticketSlice";
import cinemaSlice from "./redux/cinemaSlice";
import navigateSlice from "./redux/navigateSlice";

const root = ReactDOM.createRoot(document.getElementById("root"));

export let store = configureStore({
  reducer: {
    userSlice,
    movieSlice,
    spinnerSlice,
    modalSlice,
    ticketSlice,
    cinemaSlice,
    navigateSlice,
  },
});

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
