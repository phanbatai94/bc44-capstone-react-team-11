/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      width: {
        "1/16": "6.25%",
      },
      height: {
        "90vh": "90vh",
        tablet: "54.25vw",
        desktop: "46.25vw",
      },
      backgroundSize: {
        full: "100% 105%",
        scale: "120% 120%",
      },
    },
  },
  plugins: [],
};
